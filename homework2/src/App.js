import React, {Component} from 'react';
import './App.scss';

import Header from './components/Header'
import Button from './components/Button'
import Modal from "./components/Modal";
import GameList from "./components/GameList";


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            cartConfirm: false,
            activeGame: "",
            gamesInCart: "",
            gamesInFavorite: "",
        };
        this.addToCartHandler = this.addToCartHandler.bind(this);
        this.addToCartConfirm = this.addToCartConfirm.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.addToFavoriteHandler = this.addToFavoriteHandler.bind(this);
    }

    addToCartHandler(e) {
        this.setState({cartConfirm: true, activeGame: e.target.closest(".gameCard").dataset.vendorCode});
    }

    addToCartConfirm() {
        if (!this.state.gamesInCart.includes(this.state.activeGame)) {
            this.setState(prev => {
                    if (prev.gamesInCart.length === 0) {
                        return {...prev, gamesInCart: prev.activeGame}
                    } else if (!prev.gamesInCart.includes(prev.activeGame)) {
                        return {...prev, gamesInCart: prev.gamesInCart + ` ${prev.activeGame}`}
                    } else {
                        return prev;
                    }
                },
                () => {
                    localStorage.setItem('gamesInCart', this.state.gamesInCart);
                }
            )
        } else {
            alert("You already have this game in cart!")
        }
        this.closeModal();
    }

    closeModal() {
        this.setState({cartConfirm: false, activeGame: ""});
    }

    addToFavoriteHandler(e) {
        let game = e.target.closest(".gameCard").dataset.vendorCode;
        if (this.state.gamesInFavorite.includes(game)) {
            this.setState(prev => {
                    let newList = prev.gamesInFavorite.split(" ");
                    let index = newList.indexOf(game);
                    newList = newList.filter(el => {
                        if (el !== newList[index] && el !== "") {
                            return el
                        }
                    }).join(" ").trim();
                    return {...prev, gamesInFavorite: newList}
                },
                () => {
                    localStorage.setItem('gamesInFavorite', this.state.gamesInFavorite);
                }
            );
        } else {
            this.setState(prev => {
                    if (prev.gamesInFavorite.length === 0) {
                        return {...prev, gamesInFavorite: game}
                    } else {
                        return {...prev, gamesInFavorite: prev.gamesInFavorite + ` ${game}`}
                    }
                },
                () => {
                    localStorage.setItem('gamesInFavorite', this.state.gamesInFavorite);
                });
        }
    }

    componentDidMount() {
        fetch("./data.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        games: result.games
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        games: error.message
                    });
                }
            )

        let storagedGamesInCart = localStorage.getItem("gamesInCart");
        let storagedGamesInFavorite = localStorage.getItem("gamesInFavorite");
        if (!!storagedGamesInCart) {
            this.setState(prev => {
                return {...prev, gamesInCart: storagedGamesInCart}
            })
        }
        if (!!storagedGamesInFavorite) {
            this.setState(prev => {
                return {...prev, gamesInFavorite: storagedGamesInFavorite}
            })
        }
    }

    render() {
        const {isLoaded, games} = this.state;
        if (!isLoaded) {
            return <div>Loading...</div>
        } else {
            return (
                <section className="app">
                    <Header
                        gamesInCart={this.state.gamesInCart}
                        gamesInFavorite={this.state.gamesInFavorite}
                    />
                    <GameList
                        games={games}
                        addToCartHandler={this.addToCartHandler}
                        addToFavoriteHandler={this.addToFavoriteHandler}
                        gamesInFavorite={this.state.gamesInFavorite}
                    />
                    {this.state.cartConfirm && <Modal
                        modalName="cartConfirm"
                        closeModal={this.closeModal}
                        headerColor="a18eff"
                        header="Cart"
                        closeButton={true}
                        bodyColor="light-green"
                        text="Are you sure you want to add this game to cart?"
                        actions={
                            <>
                                <Button
                                    text={"Yup"}
                                    onClick={this.addToCartConfirm}
                                    backgroundColor={"light-blue"}
                                />
                                <Button
                                    text={"Nope"}
                                    onClick={this.closeModal}
                                    backgroundColor={"not-so-dark-red"}
                                />
                            </>
                        }
                    />}
                </section>
            )
        }
    }
}

export default App;
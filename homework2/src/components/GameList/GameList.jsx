import React, {Component} from 'react';
import PropTypes from 'prop-types';

import "./gameList.scss"
import GameCard from "./components/GameCard";

class GameList extends Component {
    render() {
        return (
            <ul className="gameList">
                {this.props.games.map((game, key) => {
                    return (
                        <GameCard
                            game={game}
                            key={key}
                            addToCartHandler={this.props.addToCartHandler}
                            addToFavoriteHandler={this.props.addToFavoriteHandler}
                            gameInFavorite={this.props.gamesInFavorite.includes(game?.vendorСode)}
                        />
                    )
                })}
            </ul>
        )
    }
}

GameList.propTypes = {
    games: PropTypes.arrayOf(PropTypes.object).isRequired,
    addToCartHandler: PropTypes.func.isRequired,
    addToFavoriteHandler: PropTypes.func.isRequired,
    gamesInFavorite: PropTypes.string.isRequired
}



export default GameList;
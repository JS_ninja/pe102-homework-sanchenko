import React, {Component} from 'react';
import PropTypes from "prop-types";

import "./addToFavorite.scss"

class AddToFavorite extends Component {
    render() {
        let icoPath = "./ico/unstar.png";
        if(this.props.inFavorite){
            icoPath = "./ico/star.png";
        }
        return (
            <div className="favorite" style={{backgroundImage: `url(${icoPath})`}} onClick={this.props.addToFavoriteHandler}></div>
        )
    }
}

AddToFavorite.propTypes = {
    inFavorite: PropTypes.bool.isRequired,
    addToFavoriteHandler: PropTypes.func.isRequired,
}

export default AddToFavorite;
import React, {Component} from 'react';
import PropTypes from "prop-types";

import "./gameCard.scss"
import Button from "../../../Button";
import AddToFavorite from "./components/AddToFavorite";


class GameCard extends Component {
    render() {
        return (
            <li className="gameCard" data-vendor-code={this.props.game.vendorСode}>
                <div className="poster" style={{backgroundImage: "url(" + this.props.game.image + ")"}}></div>
                <div className="underPoster" style={{backgroundColor: this.props.game.color}}>
                    <p className="gameTitle">{this.props.game.title}</p>
                    <p className="price">Price: {this.props.game.price}$</p>
                </div>
                <div className="description">
                    {this.props.game.description}
                    <Button
                        backgroundColor="light-blue"
                        text="Add to cart"
                        onClick={this.props.addToCartHandler}
                    />
                </div>
                <AddToFavorite
                    addToFavoriteHandler={this.props.addToFavoriteHandler}
                    inFavorite={this.props.gameInFavorite}
                />
            </li>
        )
    }
}

GameCard.propTypes = {
    game: PropTypes.object.isRequired,
    addToCartHandler: PropTypes.func.isRequired,
    addToFavoriteHandler: PropTypes.func.isRequired,
    gameInFavorite: PropTypes.bool.isRequired,
}

export default GameCard;
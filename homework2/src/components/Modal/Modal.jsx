import React, {Component} from 'react';
import PropTypes from "prop-types";

import "./modal.scss"

class Modal extends Component {
    render() {
        return (
            <div className={this.props.modalName + " underModal"} onClick={(e)=>{
                if(!e.target.closest('.modal')){
                    this.props.closeModal(e);
                }
            }}>
                <section className="modal">
                    <header className={this.props.headerColor}>{this.props.header}{this.props.closeButton && <span className="closeButton" onClick={this.props.closeModal}>x</span> }</header>
                    <article className={this.props.bodyColor}>{this.props.text}
                    <div className="buttons">
                        {this.props.actions}
                    </div>
                    </article>
                </section>
            </div>
        )
    }
}

Modal.propTypes = {
    modalName: PropTypes.string,
    closeModal: PropTypes.func.isRequired,
    headerColor: PropTypes.string,
    bodyColor: PropTypes.string,
    header: PropTypes.string,
    text: PropTypes.string,
    closeButton: PropTypes.bool,
    actions: PropTypes.node,
}

Modal.defaultProps = {
    modalName: "defaultModal",
    headerColor: "a18eff",
    bodyColor: "light-blue",
    header: "Header placeholder",
    text: "Lorem ipsum",
    closeButton: true,
}

export default Modal;
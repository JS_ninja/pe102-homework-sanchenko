import React, {Component} from 'react';
import PropTypes from 'prop-types';

import "./header.scss"

class Header extends Component {
    render() {
        let cartLength = this.props.gamesInCart.length === 0 ? 0 : this.props.gamesInCart.trim().split(" ").length;
        let favoriteLength = this.props.gamesInFavorite.length === 0 ? 0 : this.props.gamesInFavorite.trim().split(" ").length;

        return (
            <header className="siteHeader">
                <h1>Game Oasis</h1>
                <p>Escape to Gaming Paradise!</p>
                <div className="sidebar">
                    <p className="cart">{cartLength}</p>
                    <p className="favorites">{favoriteLength}</p>
                </div>
            </header>
        )
    }
}

Header.propTypes = {
    gamesInCart: PropTypes.string.isRequired,
    gamesInFavorite: PropTypes.string.isRequired,
}

export default Header;
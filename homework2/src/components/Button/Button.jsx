import React, { Component } from 'react';
import PropTypes from 'prop-types';

import "./button.scss"

class Button extends Component {
    render() {
        return(
            <button onClick={this.props.onClick} className={this.props.backgroundColor}>{this.props.text}</button>
        )
    }
}

Button.propTypes = {
    onClick: PropTypes.func.isRequired,
    backgroundColor: PropTypes.string,
    text: PropTypes.string
}

Button.defaultProps = {
    onClick: ()=>{console.error("button onClick function is not declared!")},
    backgroundColor: "light-blue",
    text: "Ok"
}

export default Button;
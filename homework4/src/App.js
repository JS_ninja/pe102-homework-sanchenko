import { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import { useDispatch, useSelector } from 'react-redux';
import * as a from "./store/actions"

import './App.scss';

import Header from './components/Header'
import Button from './components/Button'
import Modal from "./components/Modal";
import GameList from "./components/GameList";
import Cart from "./components/Cart";
import Favorites from "./components/Favorites";


function App() {
    const dispatch = useDispatch();

// ==== SELECTORS ==== //
    const isLoaded = useSelector((store) => store.isLoaded)
    const isAddModal = useSelector((store) => store.addToCartConfirm);
    const isRemoveModal = useSelector((store) => store.removeFromCartConfirm);
    const currentActive = useSelector((store) => store.activeGame);
    const currentCart = useSelector((store) => store.gamesInCart);
    const currentFavorite = useSelector((store) => store.gamesInFavorite);
    const games = useSelector((store) => store.games);
// ==== SELECTORS END ==== //

    class LocalStorageHandler {
        static gamesInCart(value) {
            localStorage.setItem("gamesInCart", value)
        };

        static gamesInFavorite(value) {
            localStorage.setItem("gamesInFavorite", value)
        };
    }

    function addToCartHandler(e) {
        const newActiveGame = e.target.closest(".gameCard").dataset.vendorCode;
        dispatch(a.SET_ACTIVE_GAME(newActiveGame))
        dispatch(a.SET_ADD_TO_CART_CONFIRM(true))
    }

    function addToCartConfirmHandler() {
        const isInCart = currentCart.split(" ").some(item => item === currentActive);
        if (isInCart) {
            console.error("This game is already in the cart!");
        } else {
            const newCartFilling = currentCart + ` ${currentActive}`;
            LocalStorageHandler.gamesInCart(newCartFilling);
            dispatch(a.UPDATE_GAMES_IN_CART(newCartFilling));
        }
        closeModal();
    }

    function removeFromCartHandler(e) {
        const gameToDel = e.target.closest(".gameInCart").dataset.vendorCode;
        dispatch(a.SET_ACTIVE_GAME(gameToDel))
        dispatch(a.SET_REMOVE_FROM_CART_CONFIRM(true));
    }

    function removeFromCartConfirmHandler() {
        console.log("removeFromCartConfirmHandler")

        const newCart = currentCart.split(" ").filter(game => {
            if (game !== currentActive) {
                return game
            }
        }).join(" ");
        LocalStorageHandler.gamesInCart(newCart);
        dispatch(a.UPDATE_GAMES_IN_CART(newCart));

        closeModal();
    }


    function closeModal() {

        dispatch(a.SET_REMOVE_FROM_CART_CONFIRM(false));
        dispatch(a.SET_ACTIVE_GAME(""));
        dispatch({type: "SET_ADD_TO_CART_CONFIRM", payload: false});
    }

    function addToFavoriteHandler(e) {
        let game = e.target.closest(".gameCard").dataset.vendorCode;

        if (currentFavorite.includes(game)) {
            let newList = currentFavorite.split(" ");
            let index = newList.indexOf(game);
            newList = newList.filter(el => {
                if (el !== newList[index] && el !== "") {
                    return el
                }
            }).join(" ").trim();
            LocalStorageHandler.gamesInFavorite(newList)
            dispatch(a.UPDATE_GAMES_IN_FAVORITE(newList))
        } else {
            let newList = currentFavorite + ` ${game}`;
            LocalStorageHandler.gamesInFavorite(newList)
            dispatch(a.UPDATE_GAMES_IN_FAVORITE(newList))
        }
    }

    useEffect(() => {
        // fetch("./data.json")
        //     .then(res => res.json())
        //     .then(
        //         (result) => {
        //             dispatch(a.UPDATE_GAMES(result.games));
        //             dispatch(a.SET_IS_LOADED());
        //         },
        //         (error) => {
        //             dispatch(a.SET_ERROR(error))
        //             dispatch(a.SET_IS_LOADED);
        //         }
        //     )

        dispatch(a.FETCH_GAMES());

        let previousGamesInCart = localStorage.getItem("gamesInCart");
        let previousGamesInFavorite = localStorage.getItem("gamesInFavorite");
        if (!!previousGamesInCart) {
            dispatch(a.UPDATE_GAMES_IN_CART(previousGamesInCart))
        }
        if (!!previousGamesInFavorite) {
            dispatch(a.UPDATE_GAMES_IN_FAVORITE(previousGamesInFavorite))
        }

    }, [])

    if (!isLoaded) {
        return <div>Loading...</div>
    } else {
        return (
            <BrowserRouter>
                <section className="app">
                    <Header
                        gamesInCart={currentCart}
                        gamesInFavorite={currentFavorite}
                    />
                    <Routes>
                        <Route path="/">
                            <Route index element={<GameList
                                addToCartHandler={addToCartHandler}
                                addToFavoriteHandler={addToFavoriteHandler}
                                gamesInFavorite={currentFavorite}
                            />}
                            />
                            <Route path={"cart"} element={<Cart
                                games={games.filter(game => {
                                    if (currentCart.includes(game.vendorCode)) {
                                        return game;
                                    }
                                })}
                                removeFromCartHandler={removeFromCartHandler}
                            />}
                            />
                            <Route path={"favorites"} element={<Favorites
                                games={games.filter(game => {
                                    if (currentFavorite.includes(game.vendorCode)) {
                                        return game;
                                    }
                                })}
                                addToCartHandler={addToCartHandler}
                                addToFavoriteHandler={addToFavoriteHandler}
                                gamesInFavorite={currentFavorite}
                            />}
                            />
                        </Route>
                    </Routes>
                    {isAddModal && <Modal
                        modalName="addToCartConfirm"
                        closeModal={closeModal}
                        headerColor="a18eff"
                        header="Cart"
                        closeButton={true}
                        bodyColor="light-green"
                        text="Are you sure you want to add this game to cart?"
                        actions={
                            <>
                                <Button
                                    text={"Yup"}
                                    onClick={addToCartConfirmHandler}
                                    backgroundColor={"light-blue"}
                                />
                                <Button
                                    text={"Nope"}
                                    onClick={closeModal}
                                    backgroundColor={"not-so-dark-red"}
                                />
                            </>
                        }
                    />}
                    {isRemoveModal && <Modal
                        modalName="removeFromCartConfirm"
                        closeModal={closeModal}
                        headerColor="a18eff"
                        header="Cart"
                        closeButton={true}
                        bodyColor="light-green"
                        text="Are you sure you want to remove this game from cart?"
                        actions={
                            <>
                                <Button
                                    text={"Yup"}
                                    onClick={removeFromCartConfirmHandler}
                                    backgroundColor={"not-so-dark-red"}
                                />
                                <Button
                                    text={"Nope"}
                                    onClick={closeModal}
                                    backgroundColor={"light-blue"}
                                />
                            </>
                        }
                    />}
                </section>
            </BrowserRouter>
        )
    }
}

export default App;
import PropTypes from "prop-types";

import "./gameInCart.scss"

function GameInCart (props) {
    return (
        <li className="gameInCart" data-vendor-code={props.game.vendorCode}>
            <div className="poster" style={{backgroundImage: "url(" + props.game.image + ")"}}></div>
            <div className="underPoster" style={{backgroundColor: props.game.color}}>
                <p className="gameTitle">{props.game.title}</p>
                <p className="price">{props.game.price}$</p>
                <div className="description">
                    {props.game.description}
                </div>
                <div className="del-btn" onClick={props.removeFromCartHandler}></div>
            </div>
        </li>
    )
}

GameInCart.propTypes = {
    game: PropTypes.object.isRequired,
    removeFromCartHandler: PropTypes.func.isRequired,
}

export default GameInCart;
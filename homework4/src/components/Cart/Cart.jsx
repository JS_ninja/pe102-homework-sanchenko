import PropTypes from 'prop-types';

import "./cart.scss"
import GameInCart from "../Cart/components/GameInCart";
import Button from "../Button";
import {connect} from "react-redux";

function Cart(props) {
    let totalCost = 0;
    props.games.forEach((game) => {
        totalCost = totalCost + +game.price;
    })

    return (<>
            <h2 className={"cart-title"}>{(props.games.length > 0) ? "Games in cart:" : "The cart is empty."}</h2>
            <ul className="gameCart">
                {props.games.map((game, key) => {
                    return (
                        <GameInCart
                            game={game}
                            key={key}
                            removeFromCartHandler={props.removeFromCartHandler}
                        />
                    )
                })}
                {(props.games.length > 1) &&
                    <div className="bottom-arrow">
                        <p>Checkout button at the bottom</p>
                    </div>
                }
            </ul>
            {(props.games.length > 0) &&
                <div className="checkout">
                    Total: {totalCost}
                    <Button
                        backgroundColor={"blue"}
                        text={"Checkout"}
                        onClick={() => {
                            props.setGamesInCart("");
                            localStorage.setItem("gamesInCart", "");
                            alert("Thank you for placing the oder!")
                        }}
                    />
                </div>
            }

        </>
    )
}

Cart.propTypes = {
    games: PropTypes.arrayOf(PropTypes.object).isRequired,
}

const mapStateToProps = (store) => {
    return {
        gamesInFavorite: store.gamesInFavorite,
    }
}

// const mapDispatchToProps = (dispatch) => {
//     return {}
// }

export default connect(mapStateToProps)(Cart)
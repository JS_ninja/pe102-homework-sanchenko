import { createReducer } from "@reduxjs/toolkit"
import {
    SET_IS_LOADED,
    SET_ERROR,
    SET_ADD_TO_CART_CONFIRM,
    SET_REMOVE_FROM_CART_CONFIRM,
    SET_ACTIVE_GAME,
    UPDATE_GAMES_IN_CART,
    UPDATE_GAMES_IN_FAVORITE,
    UPDATE_GAMES,
} from "./actions";

const initialState = {
    isLoaded: false,
    error: null,
    addToCartConfirm: false,
    removeFromCartConfirm: false,
    activeGame: "",
    gamesInCart: "",
    gamesInFavorite: "",
    games: [],
}

const reducer = createReducer(initialState, (builder) => {
    builder
        .addCase(SET_IS_LOADED, (state, action) => {
            state.isLoaded = true
        })
        .addCase(SET_ERROR, (state, action) => {
            state.error = action.payload
        })
        .addCase(SET_ADD_TO_CART_CONFIRM, (state, action) => {
            state.addToCartConfirm = action.payload
        })
        .addCase(SET_REMOVE_FROM_CART_CONFIRM, (state, action) => {
            state.removeFromCartConfirm = action.payload
        })
        .addCase(SET_ACTIVE_GAME, (state, action) => {
            state.activeGame = action.payload
        })
        .addCase(UPDATE_GAMES_IN_CART, (state, action) => {
            state.gamesInCart = action.payload
        })
        .addCase(UPDATE_GAMES_IN_FAVORITE, (state, action) => {
            state.gamesInFavorite = action.payload
        })
        .addCase(UPDATE_GAMES, (state, action) => {
            state.games = action.payload
        })
})



// function reducer(store = initialState, action) {
//     switch (action.type) {
//         case "SET_IS_LOADED": {
//             return {...store, isLoaded: true};
//         }
//         case "SET_ERROR": {
//             return {...store, error: action.payload};
//         }
//         case "SET_ADD_TO_CART_CONFIRM": {
//             return {...store, addToCartConfirm: action.payload};
//         }
//         case "SET_REMOVE_FROM_CART_CONFIRM": {
//             return {...store, removeFromCartConfirm: action.payload};
//         }
//         case "SET_ACTIVE_GAME": {
//             return {...store, activeGame: action.payload};
//         }
//         case "UPDATE_GAMES_IN_CART": {
//             return {...store, gamesInCart: action.payload};
//         }
//         case "UPDATE_GAMES_IN_FAVORITE": {
//             return {...store, gamesInFavorite: action.payload};
//         }
//         case "UPDATE_GAMES": {
//             return {...store, games: action.payload};
//         }
//         default: return store;
//     }
// }

export {reducer, initialState}
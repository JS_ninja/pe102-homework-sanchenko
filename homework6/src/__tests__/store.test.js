import { initialState, reducer } from "../store/store"
import * as a from "../store/actions"
import { SET_IS_LOADED } from "../store/actions";

describe("Reducers", () => {

    test("SET_IS_LOADED", ()=>{
        const action = {type: a.SET_IS_LOADED}
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            isLoaded: true,
        })
    })

    test("SET_ERROR", ()=>{
        const action = {
            type: a.SET_ERROR,
            payload: "Error message",
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            error: "Error message",
        })
    })

    test("SET_ERROR (empty string)", ()=>{
        const action = {
            type: a.SET_ERROR,
            payload: "",
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            error: "",
        })
    })

    test("SET_ADD_TO_CART_CONFIRM (true)", ()=>{
        const action = {
            type: a.SET_ADD_TO_CART_CONFIRM,
            payload: true,
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            addToCartConfirm: true,
        })
    })

    test("SET_ADD_TO_CART_CONFIRM (false)", ()=>{
        const action = {
            type: a.SET_ADD_TO_CART_CONFIRM,
            payload: false,
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            addToCartConfirm: false,
        })
    })

    test("SET_REMOVE_FROM_CART_CONFIRM (true)", ()=>{
        const action = {
            type: a.SET_REMOVE_FROM_CART_CONFIRM,
            payload: true,
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            removeFromCartConfirm: true,
        })
    })

    test("SET_REMOVE_FROM_CART_CONFIRM (false)", ()=>{
        const action = {
            type: a.SET_REMOVE_FROM_CART_CONFIRM,
            payload: false,
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            removeFromCartConfirm: false,
        })
    })

    test("SET_ACTIVE_GAME", ()=>{
        const action = {
            type: a.SET_ACTIVE_GAME,
            payload: "active game ID",
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            activeGame: "active game ID",
        })
    })

    test("SET_ACTIVE_GAME (empty string)", ()=>{
        const action = {
            type: a.SET_ACTIVE_GAME,
            payload: "",
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            activeGame: "",
        })
    })

    test("UPDATE_GAMES_IN_CART", ()=>{
        const action = {
            type: a.UPDATE_GAMES_IN_CART,
            payload: "games in cart IDs",
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            gamesInCart: "games in cart IDs",
        })
    })

    test("UPDATE_GAMES_IN_FAVORITE", ()=>{
        const action = {
            type: a.UPDATE_GAMES_IN_FAVORITE,
            payload: "games in favorite IDs",
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            gamesInFavorite: "games in favorite IDs",
        })
    })

    test("UPDATE_GAMES", ()=>{
        const action = {
            type: a.UPDATE_GAMES,
            payload: [{},{},{}],
        }
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            games: [{},{},{}],
        })
    })

});
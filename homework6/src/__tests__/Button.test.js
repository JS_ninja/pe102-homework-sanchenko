import {render, screen, cleanup, fireEvent} from "@testing-library/react";
import renderer from 'react-test-renderer';
import "@testing-library/jest-dom";
import Button from "../components/Button/Button";


describe("Button", () => {

    afterEach(() => {
        cleanup();
    })

    const mock = {
        text: "Mock text",
        onClick: jest.fn(),
        backgroundColor: "light-blue",
    }

    test("Component Button should render", () => {
        render(<Button
            text={mock.text}
            onClick={mock.onClick}
            backgroundColor={mock.backgroundColor}
        />);
        const button = screen.getByTestId("button");
        expect(button).toBeInTheDocument();
    })

    test("Component Button should have text", () => {
        render(<Button
            text={mock.text}
            onClick={mock.onClick}
            backgroundColor={mock.backgroundColor}
        />);
        const button = screen.getByTestId("button");
        expect(button).toHaveTextContent(mock.text);
    })

    test("Component Button should have onClick handler", () => {
        const button = render(<Button
            text={mock.text}
            onClick={mock.onClick}
            backgroundColor={mock.backgroundColor}
        />).container.firstChild;
        fireEvent.click(button);
        expect(mock.onClick).toHaveBeenCalled();
    })

    test("Matches snapshot", () => {
        const mock = {
            text: "Mock text", backgroundColor: "light-blue", onClick: () => {/* mock function */
            }
        }
        const tree = renderer
            .create(<Button
                text={mock.text}
                onClick={mock.onClick}
                backgroundColor={mock.backgroundColor}
            />).toJSON();
        expect(tree).toMatchSnapshot();
    })

})
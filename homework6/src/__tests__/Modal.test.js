import {render, screen, cleanup, fireEvent} from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import renderer from 'react-test-renderer';
import "@testing-library/jest-dom";
import Modal from "../components/Modal/Modal";

describe("Modal", () => {


    afterEach(() => {
        cleanup();
    })

    const mock = {
        modalName: "Mock modal name",
        closeModal: jest.fn(),
        headerColor: "a18eff",
        header: "Mock modal header",
        closeButton: true,
        bodyColor: "light-green",
        text: "Mock modal text content",
        actions: `<>
                <Button
                    text={"Button 1"}
                    onClick={()=>{ /* Button 1 handler */ }}
                    backgroundColor={"light-blue"}
                />
                <Button
                    text={"Button 2"}
                    onClick={()=>{ /* Button 2 handler */ }}
                    backgroundColor={"light-blue"}
                />
              </>`
    }

    test("Component Modal should render", () => {
        render(<Modal
            modalName={mock.modalName}
            closeModal={mock.closeModal}
            headerColor={mock.headerColor}
            header={mock.header}
            closeButton={mock.closeButton}
            bodyColor={mock.bodyColor}
            text={mock.text}
            actions={mock.actions}
        />);
        const modal = screen.getByTestId("modal");
        expect(modal).toBeInTheDocument();
    })

    test("Component Modal should have text", () => {
        render(<Modal
            modalName={mock.modalName}
            closeModal={mock.closeModal}
            headerColor={mock.headerColor}
            header={mock.header}
            closeButton={mock.closeButton}
            bodyColor={mock.bodyColor}
            text={mock.text}
            actions={mock.actions}
        />);
        const modal = screen.getByTestId("modal");
        expect(modal).toHaveTextContent(mock.text);
    })

    test("Component Modal should have header", () => {
        render(<Modal
            modalName={mock.modalName}
            closeModal={mock.closeModal}
            headerColor={mock.headerColor}
            header={mock.header}
            closeButton={mock.closeButton}
            bodyColor={mock.bodyColor}
            text={mock.text}
            actions={mock.actions}
        />);
        const modal = screen.getByTestId("modal");
        expect(modal).toHaveTextContent(mock.header);
    })

    test("Matches snapshot", () => {
        const tree = renderer
            .create(<Modal
                modalName={mock.modalName}
                closeModal={mock.closeModal}
                headerColor={mock.headerColor}
                header={mock.header}
                closeButton={mock.closeButton}
                bodyColor={mock.bodyColor}
                text={mock.text}
                actions={mock.actions}
            />)
            .toJSON();
        expect(tree).toMatchSnapshot();
    })

})

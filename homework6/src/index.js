import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {initialState, reducer} from "./store/store";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";

const root = ReactDOM.createRoot(document.getElementById('root'));

const store = createStore(
    reducer,
    initialState,
    applyMiddleware(thunk)
);
// store.subscribe(() => {
//     console.log(store.getState());
// });


root.render(
    <React.StrictMode>
        <Provider store={store}>
            <App/>
        </Provider>
    </React.StrictMode>
);
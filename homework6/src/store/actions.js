import { createAction } from "@reduxjs/toolkit";

export const SET_IS_LOADED = createAction("SET_IS_LOADED");

export const SET_ERROR = createAction("SET_ERROR");

export const SET_ADD_TO_CART_CONFIRM = createAction("SET_ADD_TO_CART_CONFIRM");

export const SET_REMOVE_FROM_CART_CONFIRM = createAction("SET_REMOVE_FROM_CART_CONFIRM");

export const SET_ACTIVE_GAME = createAction("SET_ACTIVE_GAME");

export const UPDATE_GAMES_IN_CART = createAction("UPDATE_GAMES_IN_CART");

export const UPDATE_GAMES_IN_FAVORITE = createAction("UPDATE_GAMES_IN_FAVORITE");

export const UPDATE_GAMES = createAction("UPDATE_GAMES");

export const FETCH_GAMES = () => (dispatch) => {
    fetch("./data.json")
        .then(res => res.json())
        .then(
            (result) => {
                dispatch(UPDATE_GAMES(result.games));
                dispatch(SET_IS_LOADED());
            },
            (error) => {
                dispatch(SET_ERROR(error))
                dispatch(SET_IS_LOADED);
            }
        )
}
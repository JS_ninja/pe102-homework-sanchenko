import "./ListViewSwitcher.scss"
import {ListType} from "../../../../App";

import {useContext} from "react";

function ListViewSwitcher() {
    let {listView, setListView, LocalStorageHandler} = useContext(ListType);
    let extraStyle = "";
    if (listView) {
        extraStyle = "listView";
    }

    return (
        <div className={"list-view-switcher " + extraStyle} onClick={() => {
            setListView(!listView);
            LocalStorageHandler.listView(!listView);
        }}>
            <div className="first"></div>
            <div className="second"></div>
            <div className="third"></div>
            <div className="fourth"></div>
            <div className="fifth"></div>
            <div className="sixth"></div>
            <div className="seventh"></div>
            <div className="eighth"></div>
            <div className="ninth"></div>
        </div>
    )
}

export default ListViewSwitcher;
import PropTypes from "prop-types";

import "./gameCard.scss"
import Button from "../../../Button";
import AddToFavorite from "./components/AddToFavorite";

import {ListType} from "../../../../App";
import {useContext} from "react";

function GameCard (props) {
    let {listView} = useContext(ListType);
    let extraStyle = "";
    if(listView){
        extraStyle = "listView";
    }

        return (
            <li className={"gameCard " + extraStyle} data-vendor-code={props.game.vendorCode}>
                <div className="poster" style={{backgroundImage: "url(" + props.game.image + ")"}}></div>
                <div className="underPoster" style={{backgroundColor: props.game.color}}>
                    <p className="gameTitle">{props.game.title}</p>
                    <p className="price">Price: {props.game.price}$</p>
                </div>
                <div className="description">
                    {props.game.description}
                    <Button
                        backgroundColor="light-blue"
                        text="Add to cart"
                        onClick={props.addToCartHandler}
                    />
                </div>
                <AddToFavorite
                    addToFavoriteHandler={props.addToFavoriteHandler}
                    inFavorite={props.gameInFavorite}
                />
            </li>
        )
}

GameCard.propTypes = {
    game: PropTypes.object.isRequired,
    addToCartHandler: PropTypes.func.isRequired,
    addToFavoriteHandler: PropTypes.func.isRequired,
    gameInFavorite: PropTypes.bool.isRequired,
}

export default GameCard;
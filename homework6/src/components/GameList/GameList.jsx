import PropTypes from 'prop-types';
import {connect} from "react-redux";

import "./gameList.scss"
import GameCard from "./components/GameCard";
import ListViewSwitcher from "./components/ListViewSwitcher";


function GameList(props) {

    return (
        <>
            <ListViewSwitcher/>
            <ul className="gameList">
                {props.games.map((game, key) => {
                    return (
                        <GameCard
                            game={game}
                            key={key}
                            addToCartHandler={props.addToCartHandler}
                            addToFavoriteHandler={props.addToFavoriteHandler}
                            gameInFavorite={props.gamesInFavorite.includes(game?.vendorCode)}
                        />
                    )
                })}
            </ul>
        </>
    )
}


GameList.propTypes = {
    addToCartHandler: PropTypes.func.isRequired,
    addToFavoriteHandler: PropTypes.func.isRequired,
}


// export default GameList;

const mapStateToProps = (store) => {
    return {
        games: store.games,
        gamesInFavorite: store.gamesInFavorite,
    }
}

// const mapDispatchToProps = (dispatch) => {
//     return {}
// }

export default connect(mapStateToProps)(GameList)
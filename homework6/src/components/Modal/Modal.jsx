import PropTypes from "prop-types";

import "./modal.scss"

function Modal(props) {
    return (
        <div className={props.modalName + " underModal"} onClick={(e) => {
            if (!e.target.closest('.modal')) {
                   props.closeModal(e);
            }
        }} data-testid="modal">
            <section className="modal">
                <header className={props.headerColor}>{props.header}{props.closeButton &&
                    <span className="closeButton" onClick={props.closeModal}>x</span>}</header>
                <article className={props.bodyColor}>{props.text}
                    <div className="buttons">
                        {props.actions}
                    </div>
                </article>
            </section>
        </div>
    )
}

Modal.propTypes = {
    modalName: PropTypes.string,
    closeModal: PropTypes.func.isRequired,
    headerColor: PropTypes.string,
    bodyColor: PropTypes.string,
    header: PropTypes.string,
    text: PropTypes.string,
    closeButton: PropTypes.bool,
    actions: PropTypes.node,
}

Modal.defaultProps = {
    modalName: "defaultModal",
    headerColor: "a18eff",
    bodyColor: "light-blue",
    header: "Header placeholder",
    text: "Lorem ipsum",
    closeButton: true,
}

export default Modal;
import PropTypes from 'prop-types';

import "./favorites.scss"
import GameCard from "../GameList/components/GameCard";
import ListViewSwitcher from "../GameList/components/ListViewSwitcher";

function Favorites(props) {
    return (<>
            <h2 className={"cart-title"}>{(props.games.length > 0) ? "Games in favorites:" : "You don't have any favorites."}</h2>
            <ListViewSwitcher/>
            <ul className="favorites">
                {props.games.map((game, key) => {
                    return (
                        <GameCard
                            game={game}
                            key={key}
                            addToCartHandler={props.addToCartHandler}
                            addToFavoriteHandler={props.addToFavoriteHandler}
                            gameInFavorite={props.gamesInFavorite.includes(game?.vendorCode)}
                        />
                    )
                })}
            </ul>
           </>
    )
}

Favorites.propTypes = {
    games: PropTypes.arrayOf(PropTypes.object).isRequired,

}

export default Favorites;
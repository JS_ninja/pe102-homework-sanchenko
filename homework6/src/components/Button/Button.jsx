import PropTypes from 'prop-types';

import "./button.scss"

function Button(props) {
    return (
        <button onClick={props.onClick} className={props.backgroundColor} data-testid="button">{props.text}</button>
    )
}

Button.propTypes = {
    onClick: PropTypes.func.isRequired,
    backgroundColor: PropTypes.string,
    text: PropTypes.string
}

Button.defaultProps = {
    onClick: () => {
        console.error("button onClick function is not declared!")
    },
    backgroundColor: "light-blue",
    text: "Ok"
}

export default Button;
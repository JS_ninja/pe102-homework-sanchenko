import { useFormik } from "formik";
import * as Yup from 'yup';
import { useDispatch } from "react-redux";
import * as a from "../../store/actions";

import "./checkout.scss"

function Checkout(props) {
    const dispatch = useDispatch();

    const validationSchema = Yup.object().shape({
        firstName: Yup.string().required('Please enter your name!').min(2, 'Too short!'),
        lastName: Yup.string().required('Please enter your surname!').min(2, 'Too short!'),
        age: Yup.number().required('Please enter your age!').max(100, 'Too much!'),
        deliveryAddress: Yup.string().required('Please enter your delivery address!').min(15, 'Minimum 15 characters!'),
        mobileNumber: Yup.string().required('Please enter your mobile phone number!').matches(/^0\d{9}$/, 'Please phone number in format 0991234567'),
    });

    const formik = useFormik({
        initialValues: {
            firstName: "",
            lastName: "",
            age: "",
            deliveryAddress: "",
            mobileNumber: "",
        },
        onSubmit: values => {
            console.log("%c========================================", "color:#9b9bff");
            console.log("Thank you for pusrchasing!");
            console.log("Id's of ordered games:");
            console.log(props.currentCart);
            console.log(`Total payment: ${props.totalCost}`)
            console.log("Your contact information:");
            console.log(`Full name: ${values.firstName} ${values.lastName} \nAge: ${values.age}\nDelivery adress: ${values.deliveryAddress}\nMobile number: ${values.mobileNumber}`)
            console.log("%c========================================", "color:#9b9bff");

            let newCart = "";
            props.LocalStorageHandler.gamesInCart(newCart);
            dispatch(a.UPDATE_GAMES_IN_CART(newCart));
        },
        validationSchema: validationSchema,
    });


    return (
        <form onSubmit={formik.handleSubmit} className="checkOutForm">
            <h3>Order details:</h3>
            {/*<label htmlFor="firstName">Name:</label>*/}
            <input
                id="firstName"
                name="firstName"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.firstName}
                placeholder="Name"
            />
            <p className="checkOutForm__errorMessage">&nbsp;
                {formik.errors.firstName && formik.touched.firstName &&
                    formik.errors.firstName}
            </p>
            {/*<label htmlFor="lastName">Surname:</label>*/}
            <input
                id="lastName"
                name="lastName"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.lastName}
                placeholder="Surname"
            />
            <p className="checkOutForm__errorMessage">&nbsp;
                {formik.errors.lastName && formik.touched.lastName &&
                    formik.errors.lastName}
            </p>
            {/*<label htmlFor="age">Age:</label>*/}
            <input
                id="age"
                name="age"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.age}
                placeholder="Age"
            />
            <p className="checkOutForm__errorMessage">&nbsp;
                {formik.errors.age && formik.touched.age &&
                    formik.errors.age}
            </p>
            {/*<label htmlFor="deliveryAddress">Delivery to:</label>*/}
            <input
                id="deliveryAddress"
                name="deliveryAddress"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.deliveryAddress}
                placeholder="Delivery Address"
            />
            <p className="checkOutForm__errorMessage">&nbsp;
                {formik.errors.deliveryAddress && formik.touched.deliveryAddress &&
                    formik.errors.deliveryAddress}
            </p>
            {/*<label htmlFor="mobileNumber">Contact phone:</label>*/}
            <input
                id="mobileNumber"
                name="mobileNumber"
                type="text"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.mobileNumber}
                placeholder="Mobile number"
            />
            <p className="checkOutForm__errorMessage">&nbsp;
                {formik.errors.mobileNumber && formik.touched.mobileNumber &&
                    formik.errors.mobileNumber}
            </p>
            <p className="total">Total: {props.totalCost}</p>
            <button type="submit">Checkout</button>
        </form>
    );
}

export default Checkout;
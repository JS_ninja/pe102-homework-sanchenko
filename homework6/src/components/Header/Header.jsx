import {Link} from "react-router-dom";
// import PropTypes from 'prop-types';

import "./header.scss"
import {connect} from "react-redux";

function Header (props) {
        let cartLength = props.gamesInCart.length === 0 ? 0 : props.gamesInCart.trim().split(" ").length;
        let favoriteLength = props.gamesInFavorite.length === 0 ? 0 : props.gamesInFavorite.trim().split(" ").length;

        return (
            <header className="siteHeader">
                <Link to="/"><h1 className="heading">Game Oasis</h1></Link>
                <p className="underHeading">Escape to Gaming Paradise!</p>
                <div className="sidebar">
                    <Link to="/cart"><p className="cart">{cartLength}</p></Link>
                    <Link to="/favorites"><p className="favorites">{favoriteLength}</p></Link>
                </div>
            </header>
        )
}

// Header.propTypes = {
//     gamesInCart: PropTypes.string.isRequired,
//     gamesInFavorite: PropTypes.string.isRequired,
// }

// export default Header;

const mapStateToProps = (store) => {
    return {
        gamesInCart: store.gamesInCart,
        gamesInFavorite: store.gamesInFavorite,
    }
}

// const mapDispatchToProps = (dispatch) => {
//     return {}
// }

export default connect(mapStateToProps)(Header)
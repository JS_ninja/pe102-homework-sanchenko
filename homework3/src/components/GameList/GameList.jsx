// import React, {Component} from 'react';
import PropTypes from 'prop-types';

import "./gameList.scss"
import GameCard from "./components/GameCard";

function GameList(props) {
    return (
            <ul className="gameList">
                {props.games.map((game, key) => {
                    return (
                        <GameCard
                            game={game}
                            key={key}
                            addToCartHandler={props.addToCartHandler}
                            addToFavoriteHandler={props.addToFavoriteHandler}
                            gameInFavorite={props.gamesInFavorite.includes(game?.vendorCode)}
                        />
                    )
                })}
            </ul>
    )
}

// class GameList extends Component {
//     render() {
//         return (
//             <ul className="gameList">
//                 {this.props.games.map((game, key) => {
//                     return (
//                         <GameCard
//                             game={game}
//                             key={key}
//                             addToCartHandler={this.props.addToCartHandler}
//                             addToFavoriteHandler={this.props.addToFavoriteHandler}
//                             gameInFavorite={this.props.gamesInFavorite.includes(game?.vendorCode)}
//                         />
//                     )
//                 })}
//             </ul>
//         )
//     }
// }

GameList.propTypes = {
    games: PropTypes.arrayOf(PropTypes.object).isRequired,
    addToCartHandler: PropTypes.func.isRequired,
    addToFavoriteHandler: PropTypes.func.isRequired,
    gamesInFavorite: PropTypes.string.isRequired
}


export default GameList;
import {useEffect, useState} from "react";
import {BrowserRouter, Routes, Route} from "react-router-dom";

import './App.scss';

import Header from './components/Header'
import Button from './components/Button'
import Modal from "./components/Modal";
import GameList from "./components/GameList";
import Cart from "./components/Cart";
import Favorites from "./components/Favorites";


function App() {
    const [error, setError] = useState(null)
    const [isLoaded, setIsLoaded] = useState(false)
    const [addToCartConfirm, setAddToCartConfirm] = useState(false)
    const [removeFromCartConfirm, setRemoveFromCartConfirm] = useState(false)
    const [activeGame, setActiveGame] = useState("")
    const [gamesInCart, setGamesInCart] = useState("")
    const [gamesInFavorite, setGamesInFavorite] = useState("")
    const [games, setGames] = useState([])


    class LocalStorageHandler {
        static gamesInCart(value) {
            localStorage.setItem("gamesInCart", value)
        };

        static gamesInFavorite(value) {
            localStorage.setItem("gamesInFavorite", value)
        };
    }

    function addToCartHandler(e) {
        setActiveGame(e.target.closest(".gameCard").dataset.vendorCode);
        setAddToCartConfirm(true);
    }

    function addToCartConfirmHandler() {
        setGamesInCart(prev => {
            const isInCart = prev.split(" ").some(item => item === activeGame)
            if (isInCart) {
                console.error("This game is already in the cart!")
                return prev
            }
            LocalStorageHandler.gamesInCart(prev + ` ${activeGame}`)
            return prev + ` ${activeGame}`
        })
        closeModal();
    }

    function removeFromCartHandler(e) {
        const gameToDel = e.target.closest(".gameInCart").dataset.vendorCode;
        setActiveGame(gameToDel);
        setRemoveFromCartConfirm(true);
    }

    function removeFromCartConfirmHandler() {
        setGamesInCart(prev => {
            const newCart = prev.split(" ").filter(game=>{if(game!==activeGame){return game}}).join(" ");
            LocalStorageHandler.gamesInCart(newCart);
            return newCart;
        });
        closeModal();
    }


    function closeModal() {
        setAddToCartConfirm(false)
        setRemoveFromCartConfirm(false)
        setActiveGame("")
    }

    function addToFavoriteHandler(e) {
        let game = e.target.closest(".gameCard").dataset.vendorCode;
        if (gamesInFavorite.includes(game)) {
            setGamesInFavorite(prev => {
                let newList = prev.split(" ");
                let index = newList.indexOf(game);
                newList = newList.filter(el => {
                    if (el !== newList[index] && el !== "") {
                        return el
                    }
                }).join(" ").trim();
                LocalStorageHandler.gamesInFavorite(newList)
                return newList
            })
        } else {
            setGamesInFavorite(prev => {
                if (prev.length === 0) {
                    LocalStorageHandler.gamesInFavorite(game)
                    return game
                } else {
                    LocalStorageHandler.gamesInFavorite(prev.gamesInFavorite + ` ${game}`)
                    return prev + ` ${game}`
                }
            })
        }
    }

    useEffect(() => {
        fetch("./data.json")
            .then(res => res.json())
            .then(
                (result) => {
                    setGames(result.games)
                    setIsLoaded(true)
                },
                (error) => {
                    setError(error)
                    setGames(error.message)
                    setIsLoaded(true)
                }
            )

        let storagedGamesInCart = localStorage.getItem("gamesInCart");
        let storagedGamesInFavorite = localStorage.getItem("gamesInFavorite");
        if (!!storagedGamesInCart) {
            setGamesInCart(storagedGamesInCart)
        }
        if (!!storagedGamesInFavorite) {
            setGamesInFavorite(storagedGamesInFavorite)
        }

    }, [])

    if (!isLoaded) {
        return <div>Loading...</div>
    } else {
        return (
            <BrowserRouter>
                <section className="app">
                    <Header
                        gamesInCart={gamesInCart}
                        gamesInFavorite={gamesInFavorite}
                    />
                    <Routes>
                        <Route path="/">
                            <Route index element={<GameList
                                games={games}
                                addToCartHandler={addToCartHandler}
                                addToFavoriteHandler={addToFavoriteHandler}
                                gamesInFavorite={gamesInFavorite}
                            />}
                            />
                            <Route path={"cart"} element={<Cart
                                games={games.filter(game => {
                                    if (gamesInCart.includes(game.vendorCode)) {
                                        return game;
                                    }
                                })}
                                removeFromCartHandler={removeFromCartHandler}
                                setGamesInCart={setGamesInCart}
                            />}
                            />
                            <Route path={"favorites"} element={<Favorites
                                games={games.filter(game => {
                                    if (gamesInFavorite.includes(game.vendorCode)) {
                                        return game;
                                    }
                                })}
                                addToCartHandler={addToCartHandler}
                                addToFavoriteHandler={addToFavoriteHandler}
                                gamesInFavorite={gamesInFavorite}
                            />}
                            />
                        </Route>
                    </Routes>
                    {addToCartConfirm && <Modal
                        modalName="addToCartConfirm"
                        closeModal={closeModal}
                        headerColor="a18eff"
                        header="Cart"
                        closeButton={true}
                        bodyColor="light-green"
                        text="Are you sure you want to add this game to cart?"
                        actions={
                            <>
                                <Button
                                    text={"Yup"}
                                    onClick={addToCartConfirmHandler}
                                    backgroundColor={"light-blue"}
                                />
                                <Button
                                    text={"Nope"}
                                    onClick={closeModal}
                                    backgroundColor={"not-so-dark-red"}
                                />
                            </>
                        }
                    />}
                    {removeFromCartConfirm && <Modal
                        modalName="removeFromCartConfirm"
                        closeModal={closeModal}
                        headerColor="a18eff"
                        header="Cart"
                        closeButton={true}
                        bodyColor="light-green"
                        text="Are you sure you want to remove this game from cart?"
                        actions={
                            <>
                                <Button
                                    text={"Yup"}
                                    onClick={removeFromCartConfirmHandler}
                                    backgroundColor={"not-so-dark-red"}
                                />
                                <Button
                                    text={"Nope"}
                                    onClick={closeModal}
                                    backgroundColor={"light-blue"}
                                />
                            </>
                        }
                    />}
                </section>
            </BrowserRouter>
        )
    }
}

export default App;
import React, {Component} from 'react';
import './App.css';

import Button from './components/Button'
import Modal from "./components/Modal";


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstModal: false,
            secondModal: false
        };
        this.openFirstModal = this.openFirstModal.bind(this)
        this.openSecondModal = this.openSecondModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }

    openFirstModal() {
        this.setState((state) => {
            return {firstModal: true}
        });
    }

    openSecondModal() {
        this.setState((state) => {
            return {secondModal: true}
        });
    }

    closeModal(e){
        this.setState((state) => {
            return {firstModal: false, secondModal: false}
        });
    }

    render() {
        return (
            <section className="app">
                <Button
                    text={"Open first modal"}
                    onClick={this.openFirstModal}
                    backgroundColor={"blue"}
                />
                <Button
                    text={"Open second modal"}
                    onClick={this.openSecondModal}
                    backgroundColor={"green"}
                />
                <Modal
                    actions={
                        <>
                            <Button
                                text={"Ok"}
                                onClick={this.closeModal}
                                backgroundColor={"dark-red"}
                            />
                            <Button
                                text={"Cancel"}
                                onClick={this.closeModal}
                                backgroundColor={"dark-red"}
                            />
                        </>
                    }
                    headerColor={"dark-red"}
                    bodyColor={"not-so-dark-red"}
                    header={"Do you want to delete this file?"}
                    text={"Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"}
                    modalName={"first"}
                    closeButton={true}
                    closeModal={this.closeModal}
                    isOpen={this.state.firstModal}
                />
                <Modal
                    actions={
                        <>
                            <Button
                                text={"Ok"}
                                onClick={this.closeModal}
                                backgroundColor={"dark-red"}
                            />
                            <Button
                                text={"Okn't"}
                                onClick={this.closeModal}
                                backgroundColor={"light-blue"}
                            />
                        </>
                    }
                    headerColor={"not-so-light-green"}
                    bodyColor={"light-green"}
                    header={"Another one modal window"}
                    text={"Click any button to continue"}
                    modalName={"second"}
                    closeButton={false}
                    closeModal={this.closeModal}
                    isOpen={this.state.secondModal}
                />
            </section>
        )
    }
}

export default App;
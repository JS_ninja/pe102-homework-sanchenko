import React, { Component } from 'react';
import "./button.scss"

class Button extends Component {

    render() {
        return(
            <button onClick={this.props.onClick} className={this.props.backgroundColor}>{this.props.text}</button>
        )
    }
}

export default Button;
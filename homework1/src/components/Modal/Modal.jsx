import React, {Component} from 'react';
import "./modal.scss"

class Modal extends Component {
    render() {
        if(this.props.isOpen){
            return (
                <div className={this.props.modalName + " underModal"} onClick={(e)=>{
                    if(!e.target.closest('.modal')){
                        this.props.closeModal(e);
                    }
                }}>
                    <section className="modal">
                        <header className={this.props.headerColor}>{this.props.header}{this.props.closeButton && <span className="closeButton" onClick={this.props.closeModal}>x</span> }</header>
                        <article className={this.props.bodyColor}>{this.props.text}
                            <div className="buttons">
                                {this.props.actions}
                            </div>
                        </article>
                    </section>
                </div>
            )
        }
    }
}

export default Modal;